const fs = require('fs');
const SelfToken = artifacts.require("SelfToken.sol");
const web3 = require("web3-utils");
const config = JSON.parse(fs.readFileSync('../config/config.json', 'utf8'));

const logger = (key, value) => {
  console.log(`   > ${key}:        ${value}`);
}

module.exports = (deployer, network, [owner]) => {

  logger("owner", owner);

  // const totalSupply = web3.toWei(`${config.initialSupply}`, "ether");

  deployer.deploy(
    SelfToken,
    config.name,
    config.symbol,
    config.decimals,
    config.initialSupply
  ).then(() =>
    SelfToken.deployed()
  ).then(() => {
    logger("SelfToken address", SelfToken.address);
  });
};
