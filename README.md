# **SelfToken** : ERC20Token and CrowdSale for the system of rewarding in Selfinity 

![img](https://gitlab.com/Zakimiii/selfinity-web/raw/recommend-item/src/app/assets/images/selfinity-logo.png)

## Welcome to SelfToken

This is a token can use in Ethereum. This will supply in mainly **Selfinity**

## Useage

#### Clone the repository

```
git clone https://github.com/Selfinity/selftoken
cd selftoken
```

### Install dependencies

````
nvm install v8.7
npm install -g yarn
yarn global add truffle
yarn install --frozen-lockfile
````

### Setting to run

````
touch .config/env.json
````

Set your MNENOMIC and INFURA_API_KEY in this file.

### Deployment to INFRA in Ropsten network

````
truffle migrate --network ropsten
truffle deploy --network ropsten
````

### Deployment to INFRA in mainnet network

```
truffle migrate --network mainnet
truffle deploy --network mainnet
```

