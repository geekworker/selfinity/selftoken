pragma solidity >=0.4.21 <0.6.0;

import "zeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "zeppelin-solidity/contracts/token/ERC20/DetailedERC20.sol";
import "zeppelin-solidity/contracts/token/ERC20/BurnableToken.sol";
import "zeppelin-solidity/contracts/token/ERC20/MintableToken.sol";
// import "zeppelin-solidity/contracts/ownership/Ownable.sol";

contract SelfToken is BurnableToken, MintableToken, DetailedERC20 {

  constructor(
    string memory _name,
    string memory _symbol,
    uint8 _decimals,
    uint256 _initialSupply
  )
    DetailedERC20(_name, _symbol, _decimals)
    public
  {
    mint(msg.sender, _initialSupply * 10 ** uint256(_decimals));
  }
}
